from datetime import datetime
import json

with open("corpus_entiers/corpus_novosti.json", "r") as file:
    donnees = json.load(file)

corpus_filtre = {}
filter_date = datetime(2020, 6, 1, 0, 0)


for id, infos in donnees.items() :
    curr_date = datetime.strptime(infos["date"], '%Y-%m-%d')
    if curr_date >= filter_date:
        corpus_filtre[id] = infos

with open("corpus_novosti_filtre.json", "w") as file:
    file.write(json.dumps(corpus_filtre, ensure_ascii = False, indent = 4))
